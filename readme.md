** Créer une image et lancer le conteneur:
 - sudo docker build -t docker_leasing_compagny .
 - sudo docker run -p 8000:8000 -i -t docker_leasing_compagny
 
** Reste à faire
 - utiliser Gunicorn comme serveur de production
 - remplacer sqlite3 par postgres
 - utilisation d'un baker dans les tests
 - créer un .yalm pour externaliser le setting de l'app
 - un front
 - ajouter de l'authentification
 - de nombreuses features
 
 Développé par Ismaël Chouraqi en 6h
 
