from django.urls import include, path
from rest_framework.routers import DefaultRouter

from leasing_app import views
from leasing_app.webservices import (
    home,
    add_new_car,
    add_new_customer,
    availability,
    rent_to_customer,
    return_car,
)

router = DefaultRouter()
router.register(r"cars", views.CarViewSet)
router.register(r"customers", views.CustomerViewSet)
router.register(r"leasing", views.LeasingViewSet)


urlpatterns = [
    path("", include(router.urls)),
    path(r"home/", home),
    path(r"car/add/", add_new_car),
    path(r"car/get_availability/", availability),
    path(r"customer/add/", add_new_customer),
    path(
        r"leasing/rent_to_customer/<customers_license_id>/<cars_license_plate>/<leasing_time>/",
        rent_to_customer,
    ),
    path(r"leasing/return_car/<customers_license_id>/", return_car),
]
