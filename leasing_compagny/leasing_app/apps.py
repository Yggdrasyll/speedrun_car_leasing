from django.apps import AppConfig


class LeasingAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "leasing_app"
