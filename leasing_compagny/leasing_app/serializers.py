from rest_framework import serializers

from leasing_app.models import (
    Car,
    Customer,
    Leasing,
)


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = "__all__"


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class LeasingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leasing
        fields = "__all__"
