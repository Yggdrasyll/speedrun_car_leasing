from django.test import TestCase, Client
from django.db.utils import IntegrityError

from leasing_app.models import Car, Customer, Leasing
from random import choices
from string import ascii_uppercase
from datetime import date


class TestCar(TestCase):
    def setUp(self):
        self.client = Client()
        self.car_payload = {
            "brand": "Citroën",
            "model": "DS",
            "license_plate": "AB123CD",
            "color": "jaune",
        }

    def test_add_new_car_restframework(self):
        response = self.client.post("/cars/", self.car_payload)
        self.assertEqual(response.status_code, 201)

        queryset = Car.objects.all()
        self.assertEqual(len(queryset), 1)

        self.car_payload["color"] = "rouge"
        response = self.client.post("/cars/", self.car_payload)
        self.assertEqual(response.status_code, 400)

        self.car_payload["license_plate"] = "ZY987XW"
        response = self.client.post("/cars/", self.car_payload)
        queryset = Car.objects.all()
        self.assertEqual(len(queryset), 2)

    def test_add_new_car(self):
        response = self.client.post("/car/add/", self.car_payload)
        self.assertEqual(response.status_code, 200)

        queryset = Car.objects.all()
        self.assertEqual(len(queryset), 1)

        self.car_payload["color"] = "rouge"
        with self.assertRaises(IntegrityError):
            response = self.client.post("/car/add/", self.car_payload)

        self.car_payload["license_plate"] = "ZY987XW"
        response = self.client.post("/car/add/", self.car_payload)
        queryset = Car.objects.all()
        self.assertEqual(len(queryset), 2)

    def test_get_availability(self):
        for cars_number in range(10):
            self.car_payload["license_plate"] = "".join(choices(ascii_uppercase, k=7))
            self.client.post("/cars/", self.car_payload)
        Car.objects.filter(id__in=[2, 3, 8]).update(leasing_status=True)
        response = self.client.get("/car/get_availability/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()["Disponible"]), 7)


class TestCustomer(TestCase):
    def setUp(self):
        self.client = Client()
        self.customer_payload = {
            "first_name": "Charles",
            "last_name": "DeGaulle",
            "license_id": "123tototiti234",
        }

    def test_add_new_customer(self):
        response = self.client.post("/customer/add/", self.customer_payload)
        self.assertEqual(response.status_code, 200)

        queryset = Customer.objects.all()
        self.assertEqual(len(queryset), 1)

        self.customer_payload["last_name"] = "LeRouge"
        with self.assertRaises(IntegrityError):
            response = self.client.post("/customer/add/", self.customer_payload)

        self.customer_payload["license_id"] = "45637tititutu"
        response = self.client.post("/customer/add/", self.customer_payload)
        queryset = Customer.objects.all()
        self.assertEqual(len(queryset), 2)


class TestLeasing(TestCase):
    def setUp(self):
        self.client = Client()
        self.customer_payload = {
            "first_name": "Charles",
            "last_name": "DeGaulle",
            "license_id": "123tototiti234",
        }
        self.car_payload_1 = {
            "brand": "Citroën",
            "model": "DS",
            "license_plate": "AB123CD",
            "color": "jaune",
        }
        self.car_payload_2 = {
            "brand": "Mercedes",
            "model": "Classe A",
            "license_plate": "TO456TO",
            "color": "verte pomme",
        }

    def test_rent_to_customer(self):
        self.client.post("/cars/", self.car_payload_1)
        self.client.post("/cars/", self.car_payload_2)

        response = self.client.get(
            "/leasing/rent_to_customer/123tototiti234/AB123CD/4/"
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "nouveau client")

        self.client.post("/customers/", self.customer_payload)
        customer = Customer.objects.get(license_id="123tototiti234")

        response = self.client.get(
            "/leasing/rent_to_customer/123tototiti234/AB123CD/4/"
        )
        self.assertEqual(response.status_code, 200)
        car = Car.objects.get(license_plate="AB123CD")
        leasing_customer = Leasing.objects.filter(customer=customer)
        leasing_car = Leasing.objects.filter(car=car)
        self.assertEqual(len(leasing_customer), 1)
        self.assertEqual(leasing_car[0], leasing_customer[0])
        self.assertEqual(car.leasing_status, True)

    def test_return_car(self):
        self.client.post("/cars/", self.car_payload_1)
        self.client.post("/customers/", self.customer_payload)
        response = self.client.get("/leasing/return_car/123tototiti234/")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pas de location")
        response = self.client.get(
            "/leasing/rent_to_customer/123tototiti234/AB123CD/4/"
        )
        response = self.client.get("/leasing/return_car/123tototiti234/")
        self.assertEqual(response.status_code, 200)
        car = Car.objects.get(license_plate="AB123CD")
        leasing = Leasing.objects.get(car=car)
        self.assertEqual(car.leasing_status, False)
        self.assertLessEqual(leasing.actual_return_date, date.today())
