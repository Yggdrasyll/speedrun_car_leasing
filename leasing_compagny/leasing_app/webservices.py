from rest_framework.decorators import api_view
from rest_framework.response import Response
from leasing_app.models import Car, Customer, Leasing
from datetime import date, timedelta


@api_view()
def home(request):
    return Response({"message": "Bienvenue sur Leasing APP"})


def add_request(obj, request):
    """
    This function take on Models Object and request from service
    and create an entry in the database from a POST method dealt as a dictionary.
    It return a JSON response
    """
    if request.method == "POST":
        formated_payload = request.data
        obj.objects.update_or_create(**formated_payload)
        return Response({"mise à jour de la base": formated_payload})
    else:
        return Response(
            {"JPR": "Win the 'post' needs the 'get' to win against the 'get'"}
        )


@api_view(["GET", "POST"])
def add_new_car(request):
    """
    A webservice to insert cars in database with a POST method.
    """
    return add_request(Car, request)


@api_view(["GET", "POST"])
def add_new_customer(request):
    """
    A webservice to insert customers in database with a POST method.
    """
    return add_request(Customer, request)


@api_view()
def availability(request):
    """
    A webservice to get all availaible cars.
    """
    available_cars = Car.objects.filter(leasing_status=False)
    return Response({"Disponible": available_cars.values()})


@api_view()
def rent_to_customer(request, customers_license_id, cars_license_plate, leasing_time):
    """
    A webservice to insert a leasing object in database. It takes customers_license_id,
    cars_license_plate and leasing_time in a GET method.
    It returns leasing start date in a JSON response.

    Immediate areas of improvement: add planification.
    """
    try:
        customer = Customer.objects.get(license_id=customers_license_id)
    except Customer.DoesNotExist:
        return Response({"message": "il s'agit d'un nouveau client"})
    car = Car.objects.get(license_plate=cars_license_plate)
    if car.leasing_status:
        last_leasing = Leasing.objects.filter(car=car).latest("leasing_date")
        due_date = last_leasing.leasing_date + timedelta(
            days=last_leasing.leasing_time_in_days
        )
        return Response(
            {
                "message": f"Cette voiture n'est pas disponible, son retour est prévu le {due_date}"
            }
        )
    payload = {
        "car": car,
        "customer": customer,
        "leasing_time_in_days": int(leasing_time),
    }
    car.leasing_status = True
    car.save()
    leasing = Leasing.objects.create(**payload)
    return Response({"mise à jour de la base": leasing.leasing_date})


@api_view()
def return_car(request, customers_license_id):
    """
    A webservice intended to return a car. It takes a customer license ID
    Immediate areas of improvement: Take into account multiple leasing with
    different due dates.
    """
    customer = Customer.objects.get(license_id=customers_license_id)
    leasing = Leasing.objects.filter(customer=customer)
    if not leasing:
        return Response({"message": "Ce client n'a pas de location en cours"})
    last_leasing = leasing.latest("leasing_date")
    due_date = last_leasing.leasing_date + timedelta(
        days=last_leasing.leasing_time_in_days
    )
    print(due_date)
    today = date.today()
    last_leasing.actual_return_date = today
    last_leasing.save()
    last_leasing.car.leasing_status = False
    last_leasing.car.save()
    if today > due_date:
        return Response({"message": "La location est restituée, avec du retard"})

    return Response({"message": today})
