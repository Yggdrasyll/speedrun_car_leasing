from django.db import models

# Create your models here.


class Car(models.Model):
    brand = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    license_plate = models.CharField(max_length=10, unique=True)
    color = models.CharField(max_length=10)
    leasing_status = models.BooleanField(default=False)


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    license_id = models.CharField(max_length=100, unique=True)
    sign_up_date = models.DateField(auto_now_add=True)


class Leasing(models.Model):
    car = models.ForeignKey(Car, on_delete=models.SET_NULL, blank=True, null=True)
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, blank=True, null=True
    )
    leasing_date = models.DateField(auto_now_add=True)
    leasing_time_in_days = models.IntegerField()
    actual_return_date = models.DateField(null=True)
