import datetime

# from django.contrib.auth.models import User
from rest_framework import viewsets

from leasing_app.models import (
    Car,
    Customer,
    Leasing,
)
from leasing_app.serializers import (
    CarSerializer,
    CustomerSerializer,
    LeasingSerializer,
)


class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = CarSerializer

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {
            "list_cars": response.data,
            "date": datetime.datetime.now().date(),
        }

        return response


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {"list_customers": response.data}

        return response


class LeasingViewSet(viewsets.ModelViewSet):
    queryset = Leasing.objects.all()
    serializer_class = LeasingSerializer

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = {"leasing_history": response.data}

        return response
