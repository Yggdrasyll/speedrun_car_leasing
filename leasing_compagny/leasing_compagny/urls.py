from django.contrib import admin
from django.urls import include, re_path
from django.urls import path


urlpatterns = [
    path("", include("leasing_app.urls")),
    path(r"admin/", admin.site.urls),
]
