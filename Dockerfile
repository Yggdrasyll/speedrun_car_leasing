FROM python:3.8.5

COPY requirements.txt /
COPY leasing_compagny /

RUN pip install -Ur requirements.txt

ENV PYTHONUNBUFFERED 1
EXPOSE 8000

CMD python ./manage.py test -v 2; python ./manage.py runserver 0.0.0.0:8000
